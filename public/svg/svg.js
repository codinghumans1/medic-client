export const closeIcon = (
  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
    <g id="그룹_1168" data-name="그룹 1168" transform="translate(-1431 -2188)">
      <g id="사각형_624" data-name="사각형 624" transform="translate(1431 2188)" fill="#fff" stroke="#000" strokeWidth="1">
        <rect width="26" height="26" stroke="none" />
        <rect x="0.5" y="0.5" width="25" height="25" fill="none" />
      </g>
      <g id="그룹_1167" data-name="그룹 1167" transform="translate(1440 2197)">
        <path
          id="패스_2286"
          data-name="패스 2286"
          d="M16535.42,2193.073l7.578,7.63"
          transform="translate(-16535.42 -2193.073)"
          fill="none"
          stroke="#000"
          strokeWidth="1"
        />
        <path
          id="패스_2287"
          data-name="패스 2287"
          d="M16543,2193.073l-7.578,7.63"
          transform="translate(-16535.42 -2193.073)"
          fill="none"
          stroke="#000"
          strokeWidth="1"
        />
      </g>
    </g>
  </svg>
);

export const circleImage = (
  <svg xmlns="http://www.w3.org/2000/svg" width="101" height="101" viewBox="0 0 101 101">
    <circle id="타원_200" data-name="타원 200" cx="50.5" cy="50.5" r="50.5" fill="#276FF0" />
  </svg>
);
