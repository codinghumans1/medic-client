import React, { useState } from "react";
import css from "./Header.module.css";
import logo from "../../public/assets/images/logo.svg";
import menu_icon from "../../public/assets/images/menu_icon.svg";
import Image from "next/image";
import Link from "next/link";
import { useTranslation } from "react-i18next";

const Header = () => {
  const [flag, setFlag] = useState(true);
  const [lang, setLang] = useState(true);
  const { t, i18n } = useTranslation();

  const langg = {
    borderBottom: "3px solid grey",
    opacity: 1,
  };
  const langg1 = { opacity: 0.5 };
  return (
    <nav className={css.navWrapper}>
      <div className="container">
        <div className={css.nav}>
          <div className={css.loggo}>
            <Link href="/" passHref>
              <Image src={logo} alt="logo" />
            </Link>
          </div>
          <div className={css.navlist_LOption}>
            <div className={css.menu1}>
              <Link href="/service" passHref>
                <span>{t("서비스 소개")}</span>
              </Link>
              <Link href="/community" passHref>
                <span>{t("커뮤니티")}</span>
              </Link>
              <Link href="/monthly-news" passHref>
                <span>{t("월간 뉴스")}</span>
              </Link>
              <Link href="/company" passHref>
                <span>{t("회사 소개")}</span>
              </Link>
              <a href="https://pf.kakao.com/_pxdnPb" target="_blank" rel="noreferrer">
                <span>{t("카카오톡 문의")}</span>
              </a>
            </div>
            <div className={css.language}>
              <span
                onClick={() => {
                  i18n.changeLanguage("kr");
                  setLang(true);
                }}
                style={lang ? langg : langg1}
              >
                KOR
              </span>
              <span
                onClick={() => {
                  i18n.changeLanguage("us");
                  setLang(false);
                }}
                style={lang ? langg1 : langg}
              >
                ENG
              </span>
            </div>
          </div>
          <div onClick={() => setFlag((prev) => !prev)} className={css.menu_icon}>
            <Image src={menu_icon} alt="img" />
          </div>
        </div>
      </div>
      {/* Drawer */}
      <div style={{ right: flag ? "-100%" : 0 }} className={css.drawer}>
        <div style={{ height: "100%" }} className="container">
          <div className={css.drawer_head}>
            <div className={css.drawerH1}>
              <span
                onClick={() => {
                  i18n.changeLanguage("kr");
                  setLang(true);
                }}
                style={lang ? langg : langg1}
              >
                KOR
              </span>
              <span
                onClick={() => {
                  i18n.changeLanguage("us");
                  setLang(false);
                }}
                style={lang ? langg1 : langg}
                className={css.eng}
              >
                ENG
              </span>
            </div>
            <span onClick={() => setFlag((prev) => !prev)} className={css.drawerH2}></span>
          </div>
          <div className={css.drawer_body}>
            <p className={css.home}>집</p>
            <div className={css.option}>
              <Link href="/service" passHref>
                <span onClick={() => setFlag((prev) => !prev)}>{t("서비스 소개")}</span>
              </Link>
              <Link href="/community" passHref>
                <span onClick={() => setFlag((prev) => !prev)}>{t("커뮤니티")}</span>
              </Link>
              <Link href="/monthly-news" passHref>
                <span onClick={() => setFlag((prev) => !prev)}>{t("월간 뉴스")}</span>
              </Link>
              <Link href="/company" passHref>
                <span onClick={() => setFlag((prev) => !prev)}>{t("회사 소개")}</span>
              </Link>
              <a href="https://pf.kakao.com/_pxdnPb" target="_blank" rel="noreferrer">
                <span>{t("카카오톡 문의")}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;
