import React from "react";
import Header from "./Header";

/**
 * @author
 * @function LayoutWrapper
 **/

export const LayoutWrapper = ({ children }) => {
  return (
    <div>
      <Header />
      {children}
    </div>
  );
};
