import i18next from "i18next";
import usa from "./usa.json";
import kor from "./kor.json";
import { initReactI18next } from "react-i18next";

i18next.use(initReactI18next).init({
  lng: "kr",
  resources: {
    us: usa,
    kr: kor,
  },
  fallbackLng: "kr",
  react: {
    useSuspense: false,
  },
});
export default i18next;
