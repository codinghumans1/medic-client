import img1 from "../../public/assets/images/main_img.png";
const landingpage = {
  text1: true,
  text2: "비영어권 외국인을 위한 의료 솔루션 메딕.",
  button: true,
  bgImage: img1,

  community: {
    text1: false,
    text2: "누구나 안전한 함께하는 사회를 만듭니다 .",
    button: false,
    bgImage: "",
  },
};

export const AboveFooterCommunity = {
  p1: "지금",
  p2: "국제화연구소와",
  p3: "합류하세요 .",
  btnFlag: true,
};
export const AboveFooterLanding = {
  p1: "한국을 찾은 외국인이",
  p2: "의료 사각지대에 놓이지 않도록",
  p3: "메딕이 함께 합니다!",
  btnFlag: false,
};

export default landingpage;
