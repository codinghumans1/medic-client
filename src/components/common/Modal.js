import React from "react";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import { closeIcon } from "../../../public/svg/svg";
import Image from "next/image";

/**
 * @author   Badal
 * @function Modal
 **/

const CustomModal = (props) => {
  const { open, onCloseModal, modalImageURL } = props;

  return (
    <div>
      <Modal open={open} onClose={onCloseModal} center closeIcon={closeIcon}>
        <Image src={modalImageURL} alt="advertisement" />
      </Modal>
    </div>
  );
};

export default CustomModal;
