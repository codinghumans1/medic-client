import React from "react";
import Image from "next/image";
import styles from "./NewsComp.module.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import NewsCompVer1 from "./NewsCompVer1";
import NewsCompVer2 from "./NewsCompVer2";

/**
 * @author
 * @function Test
 **/

const NewsComp = (props) => {
  return (
    <div style={{ backgroundColor: "#ffffff", width: "100%" }}>
      <div className="container">
        <div className={styles.img_text}>
          <div className={styles.img_con2}>UPDATE</div>
          <div className={styles.img_con1}>
            <Image
              alt="img_con1"
              src="/img/charles.png"
              width={546}
              height={657}
            />
          </div>
          <div className={styles.img_con3}>
            <Image
              alt="img_con3"
              src="/img/그룹 824@2x.png"
              width={400}
              height={200}
            />
          </div>
        </div>
        <div className={styles.img_cont}>
          <div className={styles.img_cont_new}>NEW</div>

          <Tabs className={styles.img_cont_but}>
            <TabList className={styles.img_cont_butcenter}>
              <Tab className={styles.img_cont_but1_link}>월간뉴스</Tab>
              <Tab className={styles.img_cont_but2_link}>보도자료</Tab>
            </TabList>
            <TabPanel className={styles.img_cont_butcont}>
              <NewsCompVer2 />
            </TabPanel>
            <TabPanel className={styles.img_cont_butcont}>
              <NewsCompVer1 />
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </div>
  );
};

export default NewsComp;
