import React from "react";
import Image from "next/image";
import styles from "./Fifthcomp.module.css";

/**
 * @author
 * @function Test
 **/

const Fifthcomp = (props) => {
  return (
    <div style={{ backgroundColor: "#ffffff", width: "100%" }}>
      <div className="container">
        <div className={styles.container5}>
          <div className={styles.col51}>
            <div className={styles.row511par}>
              <div className={styles.row511}>
                <a className={styles.text511}>통역 서비스</a>
              </div>
              <div className={styles.row512}>
                <a className={styles.text512}>
                  부담없는 가격으로<br></br> 자가 병원 방문을 돕는 <br></br>
                  <a className={styles.text5121}>영상통화 연결 .</a>
                </a>
              </div>
            </div>
            <div className={styles.row513par}>
              <div className={styles.row513}>
                <a className={styles.text513}>영상통화 보호 대리자</a>
              </div>
              <div className={styles.row514}>
                <a className={styles.text514}>
                  사용자가 병원에서 소통할 때 도움을 주는 보호 대리자와<br></br>{" "}
                  영상통화가 연결됩니다. 한국인 선배부터 도우미 <br></br>
                  선생님까지 여러분의 자가방문을 도와줍니다.
                </a>
              </div>
              <div className={styles.row515}>
                <a className={styles.text515}>더 알아보기</a>
                <Image
                  alt="img516"
                  src="/img/더보기 아이콘@2x.png"
                  width={27}
                  height={27}
                />
              </div>
            </div>
          </div>
          <div className={styles.col52}>
            <div className={styles.col522}>
              <Image
                alt="comp5"
                src="/img/그룹 719.png"
                width={370}
                height={650}
              />
            </div>
            <div className={styles.col521}>
              <Image
                alt="comp5"
                src="/img/bluecir.svg"
                width={1500}
                height={1500}
              />
            </div>
            <div className={styles.col5221}>
              <div className={styles.row513D}>
                <a className={styles.text513D}>
                  영상통화 <br></br> 보호 대리자
                </a>
              </div>
              <div className={styles.row514D}>
                <a className={styles.text514D}>
                  사용자가 병원에서 소통할 때 도움을 주는 보호 대리자와<br></br>{" "}
                  영상통화가 연결됩니다. 한국인 선배부터 도우미 <br></br>
                  선생님까지 여러분의 자가방문을 도와줍니다.
                </a>
              </div>
              <div className={styles.row515D}>
                <a className={styles.text515D}>더 알아보기</a>
                <Image
                  alt="img516"
                  src="/img/더보기 아이콘@2x.png"
                  width={20}
                  height={20}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Fifthcomp;
