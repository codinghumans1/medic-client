import css from "./SubPage_5.module.css";
import Image from "next/image";

const SubPage_5 = (props) => {
  return (
    <div style={{ backgroundColor: "#ffffff", width: "100%" }}>
      <div className="container">
        <div className={css.subpage5}>
          <div className={css.col1}>
            <div className={css.row11}>
              <span className={css.text11}>통역 서비스</span>
              <span className={css.text12}>
                부담없는 가격으로 <br></br>
                자가 병원 방문을 돕는 <br></br>
                <span className={css.text12b}>영상통화 연결 .</span>
              </span>
            </div>
            <div className={css.row12}>
              <div className={css.row12b}>
                <Image
                  alt="comp5"
                  src="/img/bluecir.svg"
                  width={670}
                  height={670}
                />
              </div>
              <div className={css.row12m}>
                <Image
                  alt="comp5"
                  src="/img/그룹 719.png"
                  width={460}
                  height={850}
                />
              </div>
            </div>
          </div>
          <div className={css.col2}>
            <div className={css.row21}>
              <Image
                alt="comp5"
                src="/img/pepacc.png"
                width={434}
                height={873}
              />
            </div>
            <div className={css.row22}>
              <span className={css.text21}>영상통화 <br className={css.text21br}></br> 보호 대리자</span>
              <span className={css.text22}>
                사용자가 병원에서 소통할 때 도움을 주는 보호 대리자와 영상통화가
                연결됩니다. 한국인 선배부터 도우미 선생님까지 여러분의
                자가방문을 도와줍니다.
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubPage_5;
