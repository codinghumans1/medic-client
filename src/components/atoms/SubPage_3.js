import css from "./SubPage_3.module.css";
import Image from "next/image";

const SubPage_3 = (props) => {
  return (
    <div style={{ backgroundColor: "#ffffff", width: "100%" }}>
      <div className="container">
        <div className={css.subpage3}>
          <div className={css.row1}>
            <div className={css.row11}>
              누구나 <br></br> 어디서나 <br></br> 메딕으로
            </div>
            <div className={css.row12}>
              <Image
                alt="image"
                src="/img/docnur.png"
                width={612}
                height={860}
              />
            </div>
          </div>
          <div className={css.row2}>
            <Image
              alt="image"
              src="/img/mobERS.png"
              width={1551}
              height={873}
            />
          </div>
          <div className={css.row3}>
            <Image alt="image" src="/img/logoCol.png" width={73} height={47} />
          </div>
          <div className={css.row4}>
            의료 솔루션 <br></br>
            <span style={{ fontWeight:"bold"}}> 메딕</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubPage_3;
