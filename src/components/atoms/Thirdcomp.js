import React from "react";
import Image from "next/image";
import styles from "./Thirdcomp.module.css";
import { useTranslation } from "react-i18next";

/**
 * @author
 * @function Thirdcomp
 **/

const Thirdcomp = (props) => {
  const { t } = useTranslation();
  return (
    <div className={styles.container3}>
      <div className={styles.col1}>
        <div className={styles.text3}>
          <div className={styles.text3_1}>
            <a className={styles.text3_11}>
              {t("메딕은 ,")} <br></br>
            </a>
          </div>
          <div className={styles.text3_2}>
            <a className={styles.text3_21}>
              Korean Life Medic은 비영어권 외국인의 <br></br>
              의료권 보장을 목적으로 외국인 의료 생활화를 위해 <br></br>다양한 맞춤 서비스를 제공합니다
            </a>
          </div>
        </div>
      </div>
      <div className={styles.col2}>
        <div className={styles.comp3}>
          <Image alt="comp3" src="/img/그룹 3@2x.png" width={846} height={988} />
        </div>
      </div>
    </div>
  );
};

export default Thirdcomp;
