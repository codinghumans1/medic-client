import React from "react";
import Image from "next/image";
import styles from "./NewsComp.module.css";
import Link from "next/link";

const NewsCompVer1 = (props) => {
  return (
    <div className={styles.img_cont_but1}>
      <div className={styles.img_cont_but1_title}>
        언론 속의 <br></br>국제화연구소
      </div>
      <div className={styles.img_cont_but1_cont}>
        <a href="https://www.joongang.co.kr/article/24088661#home" target="_blank" rel="noreferrer">
          <div className={styles.img_cont_but1_1}>
            <div className={styles.img_cont_but1_11}>
              중앙일보
              <a className={styles.img_cont_but1_111}>2021. 06. 23</a>
            </div>
            <div className={styles.img_cont_but1_12}>
              사회 문제 해결 위한<br className={styles.removebr}></br> 혁신 아이디어 ‘예비창업패키지 소셜벤처’에 선정
              <div className={styles.img_cont_but1_122}>
                <Image alt="img_cont122" src="/img/그룹 1188@2x.png" width={20} height={20} />
              </div>
            </div>
          </div>
        </a>
        <a href="http://viva100.com/mobile/view.php?key=20210810010002404" target="_blank" rel="noreferrer">
          <div className={styles.img_cont_but1_2}>
            <div className={styles.img_cont_but1_11}>
              브릿지경제
              <a className={styles.img_cont_but1_111}>2021. 08. 13</a>
            </div>

            <div className={styles.img_cont_but1_12}>
              사회 문제 해결 위한<br></br> 혁신 아이디어 ‘예비창업패키지 소셜벤처’에 선정
              <div className={styles.img_cont_but1_122}>
                <Image alt="img_cont122" src="/img/그룹 1188@2x.png" width={20} height={20} />
              </div>
            </div>
          </div>
        </a>
        <a href="https://www.syu.ac.kr/blog/sw동아리-국제화연구소-전국-대학생-sw창업-아이디어/" target="_blank" rel="noreferrer">
          <div className={styles.img_cont_but1_3}>
            <div className={styles.img_cont_but1_11}>
              삼육대학교
              <a className={styles.img_cont_but1_111}>2021. 12. 14</a>
            </div>

            <div className={styles.img_cont_but1_12}>
              외국인 의료 체증, 픽토그램으로 ‘뻥’
              <div className={styles.img_cont_but1_122}>
                <Image alt="img_cont122" src="/img/그룹 1188@2x.png" width={20} height={20} />
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
};

export default NewsCompVer1;
