import React from "react";
import Image from "next/image";
import styles from "./Eighthcomp.module.css";

/**
 * @author
 * @function Test
 **/

const Eighthcomp = (props) => {
  return (
    <div style={{ backgroundColor: "#f8f8f8" }}>
      <div className="container">
        <div className={styles.container8}>
          <div className={styles.col51}>
            <div className={styles.row511par}>
              <div className={styles.row511}>
                <a className={styles.text511}>의료 지도</a>
              </div>
              <div className={styles.row512}>
                <a className={styles.text512}>
                  <a className={styles.text5121}>알고리즘 의료 지도 </a>
                  <br></br> 알맞는 의료 기관을 <br></br> 추천해드려요 <a className={styles.text5121}>.</a>
                </a>
              </div>
            </div>
            <div className={styles.row513par}>
              <div className={styles.row513}>
                <a className={styles.text513}>의료기관 추천 서비스</a>
              </div>
              <div className={styles.row514}>
                <a className={styles.text514}>
                  사용자의 병원후기 데이터를 알고리즘 기술을 이용하여 <br></br> 의료기관을 추천하는 서비스입니다. 건강상태, <br></br> 의료비용,
                  건강정보, 의료기관의 <br></br> 글로벌정도 등을 고려합니다.
                </a>
              </div>
              <div className={styles.row515}>
                <a className={styles.text515}> 더 알아보기 </a>
                <Image alt="img516" src="/img/더보기 아이콘@2x.png" width={27} height={27} />
              </div>
            </div>
          </div>
          <div className={styles.col52}>
            <div className={styles.col522}>
              <Image alt="comp5" src="/img/의료 지도 UI@2x.png" width={370} height={650} />
            </div>
            <div className={styles.col521}>
              <Image alt="comp5" src="/img/bluecir.svg" width={1500} height={1500} />
            </div>
            <div className={styles.col5221}>
              <div className={styles.row513D}>
                <a className={styles.text513D}>의료기관 추천 서비스</a>
              </div>
              <div className={styles.row514D}>
                <a className={styles.text514D}>
                  사용자의 병원후기 데이터를 알고리즘 기술을 이용하여 <br></br> 의료기관을 추천하는 서비스입니다. 건강상태, <br></br> 의료비용,
                  건강정보, 의료기관의 <br></br> 글로벌정도 등을 고려합니다.
                </a>
              </div>
              <div className={styles.row515D}>
                <a className={styles.text515D}>더 알아보기</a>
                <Image alt="img516" src="/img/더보기 아이콘@2x.png" width={20} height={20} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Eighthcomp;
