import css from "./SubPage_2.module.css";
import Image from "next/image";

const SubPage_2 = (props) => {
  return (
    <div style={{ backgroundColor: "#ffffff", width: "100%" }}>
      <div className="container">
        <div className={css.subpage2}>
          <div className={css.title}>
            이주민의 <br className={css.sp2br}></br> 의료생활 <br className={css.sp2br}></br> 문제를 찾다
          </div>
          <div className={css.content}>
            <div className={css.row}>
              <div className={css.image1}>
                <Image alt="image" src="/img/moneyhand.png" width={180} height={180} />
              </div>
              <div className={css.text1}>
                <div className={css.t1title}>의료비 부담</div>
                <div className={css.text1content}>
                  의료 관광 코디네이터 , 의료 통역 등 언어서비스가 제공되는
                  <br className={css.sp2br2}></br>
                  병원급 의료기관에 대한 의료비 부담
                </div>
              </div>
            </div>
            <div className={css.row}>
              <div className={css.image1}>
                <Image alt="image" src="/img/infohand.png" width={180} height={180} />
              </div>
              <div className={css.text1}>
                <div className={css.t1title}>의원정보 부족</div>
                <div className={css.text1content}>
                  기존 지도 서비스는 이용자의 증상에 맞는 병원을 찾기 어렵고
                  <br className={css.sp2br2}></br>
                  의료체계가 국가마다 달라 혼선이 발생
                </div>
              </div>
            </div>
            <div className={css.row}>
              <div className={css.image1}>
                <Image alt="image" src="/img/nohand.png" width={180} height={180} />
              </div>
              <div className={css.text1}>
                <div className={css.t1title}>의료정보 이해 부족</div>
                <div className={css.text1content}>
                  번역 AI는 증상에 대한 정확한 전달이 어렵고 환자가 의사의
                  <br className={css.sp2br2}></br>
                  처방을 이해하지 못하는 문제 발생
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubPage_2;
