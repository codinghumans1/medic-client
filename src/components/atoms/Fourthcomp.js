import React from "react";
import Image from "next/image";
import styles from "./Fourthcomp.module.css";
import { useTranslation } from "react-i18next";
/**
 * @author
 * @function Fourthcomp
 **/

const Fourthcomp = (props) => {
  const { t } = useTranslation();
  return (
    <div style={{ backgroundColor: "#ffffff", width: "100%" }}>
      <div className="container">
        <div className={styles.container4}>
          <div className={styles.row41}>
            <a className={styles.text41}>Our service</a>
          </div>
          <div className={styles.row42}>
            <a className={styles.text42}>
              메딕만의 <br></br>외국인 의료 서비스 <a className={styles.text421}>.</a>
            </a>
          </div>
          <div className={styles.row43parent}>
            <div className={styles.row43}>
              <div className={styles.img431}>
                <Image alt="img4311" src="/img/보호 대리자@2x.png" width={134} height={134} />
              </div>
              <div className={styles.textwid}>
                <div className={styles.text441}>
                  <a className={styles.text4411}>{t("보호 대리자")}</a>
                </div>
                <div className={styles.text451}>
                  <a className={styles.text4511}>{t("어디서든 보호 대리자와 영상통화로 함께 진료")}</a>
                </div>
              </div>
            </div>
            <div className={styles.row44}>
              <div className={styles.img431}>
                <Image alt="img4312" src="/img/간단한 119신고@2x.png" width={134} height={134} />
              </div>
              <div className={styles.textwid}>
                <div className={styles.text441}>
                  <a className={styles.text4411}>{t("간단한 119신고")}</a>
                </div>
                <div className={styles.text451}>
                  <a className={styles.text4511}>{t("응급상황에 빠른 대처가 가능한 119 신고 과정")}</a>
                </div>
              </div>
            </div>

            <div className={styles.row45}>
              <div className={styles.img431}>
                <Image alt="img4312" src="/img/abc.png" width={134} height={134} />
              </div>
              <div className={styles.textwid}>
                <div className={styles.text441}>
                  <a className={styles.text4411}>{t("의료 픽토그램")}</a>
                </div>
                <div className={styles.text451}>
                  <a className={styles.text4511}>{t("의료 픽토그램을 이용한 편리한 의료진 상담")}</a>
                </div>
              </div>
            </div>
            <div className={styles.row46}>
              <div className={styles.img431}>
                <Image alt="img4314" src="/img/알고리즘 지도@2x.png" width={134} height={134} />
              </div>
              <div className={styles.textwid}>
                <div className={styles.text441}>
                  <a className={styles.text4411}>{t("알고리즘 지도")}</a>
                </div>
                <div className={styles.text451}>
                  <a className={styles.text4511}>{t("맞춤형 알고리즘 지도로 인근 병원 검색")}</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Fourthcomp;
