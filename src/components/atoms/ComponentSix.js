import React from 'react';
import styles from './ComponentSix.module.css';

/**
 * @author
 * @function ComponentSix
 **/

const ComponentSix = (props) => {
  return (
    <div style={{ width: '100%', backgroundColor: '#F8F8F8' }}>
      <div style={{ backgroundColor: '#F8F8F8', marginLeft: '300px' }}>
        <p>HELLO DEVELOPERS</p>
        <p>HELLO DEVELOPERS</p>
        <p>HELLO DEVELOPERS</p>
      </div>
      <div className={styles.card_wrapper}>
        <div className={styles.cards}>
          <div>HELLO IMAGE</div>
        </div>
        <div className={styles.cards}>
          <div>HELLO IMAGE</div>
        </div>
      </div>
    </div>
  );
};

export default ComponentSix;
