import css from "./SubPage_8.module.css";
import Image from "next/image";

const SubPage_8 = (props) => {
  return (
    <div style={{ backgroundColor: "##F6F6F6", width: "100%" }}>
      <div className="container">
        <div className={css.subpage8}>
          <div className={css.row1}>
            <span className={css.text11}>의료 지도</span>
            <span className={css.text12}>
              <span style={{ color: "#4A6CDD" }}>알고리즘 의료 지도 </span>
              <br></br> 알맞는 의료 기관을 <br></br>
              추천해드려요 .
            </span>
          </div>
          <div className={css.row2}>
            <span className={css.text21}>
              <span style={{ opacity: "30%" }} className={css.text211}>
                의료비
              </span>
              <br></br>
              <span style={{ opacity: "50%" }} className={css.text212}>
                의료 환경
              </span>
              <br></br>
              <span style={{ opacity: "70%" }} className={css.text213}>
                건강 상태
              </span>
              <br></br>
              <span style={{ opacity: "100%" }} className={css.text214}>
                치료 시스템
              </span>
            </span>
            <span className={css.text22}>
              <div className={css.col21b}>
                <Image
                  alt="comp5"
                  src="/img/bluecir.svg"
                  width={550}
                  height={550}
                />
              </div>
              <div className={css.col21m}>
                <Image
                  alt="comp5"
                  src="/img/의료 지도 UI@2x.png"
                  width={350}
                  height={600}
                />
              </div>
            </span>
            <span className={css.text23}>
              모두 <br></br> 중요하니까
            </span>
          </div>
          <div className={css.row3}>
            <span className={css.text31}>의료기관 추천 서비스</span>
            <span className={css.text32}>
              사용자의 병원후기 데이터를 알고리즘 기술을 이용하여{" "}
              <br className={css.text32br}></br> 의료기관을 추천하는
              서비스입니다. 건강상태, <br className={css.text32br}></br>{" "}
              의료비용, 건강정보, 의료기관의 <br className={css.text32br}></br>{" "}
              글로벌정도 등을 고려합니다.
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubPage_8;
