import React from "react";
import Image from "next/image";
import css from "./AboveFooter.module.css";
import img516 from "../../../public/img/메딕 아이콘_아이콘@3x.png";

const AboveFooter = () => {
  return (
    <div className={css.abovefooter}>
      <div
        style={{
          height: "100%",
          display: "flex",
          alignItems: "center",
        }}
        className="container"
      >
        <div className={css.div_parent}>
          <div className={css.abovefooter1}>KOREAN LIFE MEDIC</div>
          <div className={css.abovefooter2}>
            <p>한국을 찾은 외국인이</p>
            <p>의료 사각지대에 놓이지 않도록</p>
            <p>메딕이 함께 합니다!</p>
          </div>
          <div className={css.abovefooter3}>
            <Image src={img516} alt="img516" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboveFooter;
