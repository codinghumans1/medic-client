import React, { useState } from "react";
import Image from "next/image";
import styles from "./NewsComp.module.css";
import CustomModal from "../common/Modal";
import Modal1Image from "../../../public/img/Modal1Image.png";
import Modal2Image from "../../../public/img/Modal2Image.png";
import Modal3Image from "../../../public/img/Modal3Image.png";
import Modal4Image from "../../../public/img/Modal4Image.png";
import Modal5Image from "../../../public/img/Modal5Image.png";

const NewsCompVer2 = (props) => {
  /**
   * state and function to handle Modal
   */
  const [open, setOpen] = useState(false);
  const onOpenModal = () => setOpen(true);
  const onCloseModal = () => setOpen(false);
  const [modalImageURL, setModalImageURL] = useState("");

  return (
    <div className={styles.img_cont_but2}>
      <CustomModal
        open={open}
        onCloseModal={onCloseModal}
        modalImageURL={modalImageURL}
      />
      <div className={styles.img_cont_but1_title}>
        이주민의 <br></br>행복을 위해 노력하는 <br></br> 국제화연구소
      </div>
      <div className={styles.text2021}>2021</div>
      <div className={styles.img_cont_but2_cont}>
        <div className={styles.img_cont_but2_1}>
          <div className={styles.img_cont_but2_11}>
            <span className={styles.img_cont_but2_111}>8월 </span>월간뉴스
          </div>
          <div className={styles.img_cont_but2_12}>
            이주민의 행복을 위해 노력하는<br className={styles.removebr1}></br>{" "}
            국제화 연구소의 월간뉴스 8월호입니다.
            <div className={styles.img_cont_but2_122}>
              <Image
                alt="img_cont122"
                src="/img/그룹 1178@3x.png"
                width={30}
                height={30}
                onClick={() => {
                  setModalImageURL(Modal1Image);
                  onOpenModal();
                }}
              />
            </div>
          </div>
        </div>
        <div className={styles.img_cont_but2_1}>
          <div className={styles.img_cont_but2_11}>
            <span className={styles.img_cont_but2_111}>9월 </span>월간뉴스
          </div>
          <div className={styles.img_cont_but2_12}>
            이주민의 행복을 위해 노력하는 <br className={styles.removebr1}></br>{" "}
            국제화 연구소의 월간뉴스 8월호입니다.
            <div className={styles.img_cont_but2_122}>
              <Image
                alt="img_cont122"
                src="/img/그룹 1178@3x.png"
                width={30}
                height={30}
                onClick={() => {
                  setModalImageURL(Modal2Image);
                  onOpenModal();
                }}
              />
            </div>
          </div>
        </div>
        <div className={styles.img_cont_but2_1}>
          <div className={styles.img_cont_but2_11}>
            <span className={styles.img_cont_but2_111}>10월 </span>월간뉴스
          </div>
          <div className={styles.img_cont_but2_12}>
            이주민의 행복을 위해 노력하는<br className={styles.removebr1}></br>{" "}
            국제화 연구소의 월간뉴스 8월호입니다.
            <div className={styles.img_cont_but2_122}>
              <Image
                alt="img_cont122"
                src="/img/그룹 1178@3x.png"
                width={30}
                height={30}
                onClick={() => {
                  setModalImageURL(Modal3Image);
                  onOpenModal();
                }}
              />
            </div>
          </div>
        </div>
        <div className={styles.img_cont_but2_1}>
          <div className={styles.img_cont_but2_11}>
            <span className={styles.img_cont_but2_111}>11월 </span>월간뉴스
          </div>
          <div className={styles.img_cont_but2_12}>
            이주민의 행복을 위해 노력하는<br className={styles.removebr1}></br>{" "}
            국제화 연구소의 월간뉴스 8월호입니다.
            <div className={styles.img_cont_but2_122}>
              <Image
                alt="img_cont122"
                src="/img/그룹 1178@3x.png"
                width={30}
                height={30}
                onClick={() => {
                  setModalImageURL(Modal4Image);
                  onOpenModal();
                }}
              />
            </div>
          </div>
        </div>
        <div className={styles.img_cont_but2_1}>
          <div className={styles.img_cont_but2_11}>
            <span className={styles.img_cont_but2_111}>12월 </span>월간뉴스
          </div>
          <div className={styles.img_cont_but2_12}>
            이주민의 행복을 위해 노력하는<br className={styles.removebr1}></br>{" "}
            국제화 연구소의 월간뉴스 8월호입니다.
            <div className={styles.img_cont_but2_122}>
              <Image
                alt="img_cont122"
                src="/img/그룹 1178@3x.png"
                width={30}
                height={30}
                onClick={() => {
                  setModalImageURL(Modal5Image);
                  onOpenModal();
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsCompVer2;
