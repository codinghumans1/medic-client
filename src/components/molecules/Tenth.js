import React from "react";
import css from "../../../styles/Tenth.module.css";
import Image from "next/image";
import useWindowDimensions from "../../hooks/useWindowDimensions";
import c10 from "../../../public/assets/images/c10.png";
import c10_2 from "../../../public/assets/images/c10_2.png";
import splash from "../../../public/assets/images/splash_logo.png";
import click_logo from "../../../public/assets/images/click_logo.png";
const Tenth = () => {
  const { height, width } = useWindowDimensions();

  return (
    <div className={css.tenth}>
      <div className="container">
        <div style={{ width: "100%", display: "flex" }}>
          <div className={css.tenth1}>
            <p className={css.p1}>비영어권</p>
            <p className={css.p2}>외국인을 위한</p>
            <p className={css.p3}>의료 솔루션.</p>
          </div>
          <div className={css.rightp}>
            <div className={css.tenthh2}>
              <div className={css.c1}>
                <Image src={c10} alt="c10" />
              </div>
              <div className={css.c2}>
                <Image src={c10_2} alt="c10_2" />
                <div className={css.splash1}>
                  <Image src={splash} alt="splash" />
                </div>
              </div>
            </div>
            <div className={css.clickable}>
              <div className={css.text}>지금 바로 메딕을 체험해 보세요</div>
              <div className={css.click_logo}>
                <Image src={click_logo} alt="click_logo" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tenth;
