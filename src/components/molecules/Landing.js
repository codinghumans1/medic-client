import React from "react";
import css from "./Landing.module.css";
import Image from "next/image";
import img1 from "../../../public/assets/images/sweet.svg";
import Slider from "react-slick";
import { useTranslation } from "react-i18next";

const Landing = (props) => {
  var settings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    speed: 500,
  };
  const { t } = useTranslation();

  const { text1, text2, btn } = props;

  return (
    <Slider {...settings}>
      <div className={css.landing}>
        <div className="container">
          <div className={css.texts_parent}>
            <div className={css.text2}>{t(text2)}</div>
            <div className={css.btn}>
              <button>{t("앱 다운로드")}</button>
            </div>
          </div>
          <div className={css.img1}>
            <Image src={img1} alt="img" />
          </div>
          <div className={css.scroll}>Scroll</div>
        </div>
      </div>
    </Slider>
  );
};

export default Landing;
