import React from "react";
import css from "../../../styles/Nineth.module.css";
import img from "../../../public/assets/images/mobile.png";
import img1 from "../../../public/assets/images/main_mockup2.png";
import icon from "../../../public/assets/images/icon.png";
import Image from "next/image";

const Nineth = () => {
  return (
    <div style={{ backgroundColor: "#ffff" }}>
      <div className="container">
        <div className={css.nineth}>
          <div className={css.text}>
            <span>Medic Solution</span>
          </div>
          <div className={css.nineth1}>
            <div className={css.medic}>
              <div className={css.arrow}>
                <Image src={img1} alt="img" />
              </div>
              <span>Medic main page</span>
            </div>
            <div className={css.mobile}>
              <Image src={img} alt="img" />
            </div>
            <div className={css.icon}>
              <Image src={icon} alt="icon" />
            </div>
            <div className={css.report}>
              <button>Emergency Report Service</button>
            </div>
            <div className={css.medical}>
              <button>Medical Pictogram Service</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Nineth;
