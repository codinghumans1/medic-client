import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/sweet.svg";
import css from "../../../styles/Service1.module.css";

const Service1 = () => {
  return (
    <div className={css.service1}>
      <div className="container">
        <div className={css.service11}>
          <div className={css.service111}>
            병원에서 의원으로
            <br /> 외국인 의료의 생활화
          </div>
          <div className={css.service_mob111}>
            병원에서 의원으로
            <br /> 외국인
            <br /> 의료의 생활화
          </div>
          <button className={css.btn}>앱 다운로드</button>
        </div>
        <div className={css.img1}>
          <Image src={img1} alt="img" />
        </div>
        <div className={css.scroll}>Scroll</div>
      </div>
    </div>
  );
};

export default Service1;
