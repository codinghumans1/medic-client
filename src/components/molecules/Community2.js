import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/community21.png";
import css from "../../../styles/Community2.module.css";
const Community2 = () => {
  return (
    <div className={css.community2}>
      <div className="container">
        <div className={css.community21}>
          함께 하는
          <br /> 사회를 위해
          <br /> 노력하는 국제화연구소는
        </div>
        <div className={css.community22}>
          <div className={css.community221}>
            <Image src={img1} alt="img1" />
          </div>
          <div className={css.community222}>
            <div className={css.community2221}>
              전세계 인구 증가율 보다 빠르게 증가하는 국제 이주민 수와
              <br /> 이주민이 급증하고 있는 한국 사회의 해답을 찾기 위해서
              시작되었습니다.
              <br /> 우리는 국내 외국인의 주민화 대제의 첫 시작을 외국인
              의료안전으로 합니다.
            </div>
          </div>
        </div>
      </div>
      {/* Mobile */}
      <div className={css.com_mob2}>
        <div className={css.com_mob21}>
          함께 하는
          <br />
          사회를 위해 노력하는
          <br />
          국제화연구소는
        </div>
        <div className={css.com_mob22}>
          <Image src={img1} alt="img1" />
        </div>
        <div className={css.com_mob23}>
          빠르게 증가하는 국제 이주민 수와
          <br />
          이주민이 급증하고 있는 한국 사회의 해답을
          <br />
          찾기 위해서 시작되었습니다. 우리는 국내 외국인의
          <br /> 주민화 대제의 첫 시작을 외국인 의료안전으로 합니다.
        </div>
      </div>
    </div>
  );
};

export default Community2;
