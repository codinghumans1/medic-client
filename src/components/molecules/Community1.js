import React from "react";
import css from "./Community1.module.css";
import Image from "next/image";
import img1 from "../../../public/assets/images/sweet.svg";

const Landing = () => {
  return (
    <div className={css.landing}>
      <div className="container">
        <div className={css.texts_parent}>
          누구나 안전한
          <br /> 함께하는 사회를
          <br /> 만듭니다 .
        </div>
        {/* Mobile */}
        <div className={css.comm_mob11}>
          누구나
          <br />
          안전한
          <br />
          사회를 만듭니다 .
        </div>
        <div className={css.img1}>
          <Image src={img1} alt="img" />
        </div>
        <div className={css.scroll}>Scroll</div>
      </div>
    </div>
  );
};

export default Landing;
