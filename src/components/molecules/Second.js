import React from "react";
import Image from "next/image";
import css from "../../../styles/Second.module.css";
import img1 from "../../../public/assets/images/img1.png";
import img2 from "../../../public/assets/images/img2.png";
import img3 from "../../../public/assets/images/img3.png";
import img4 from "../../../public/assets/images/img4.png";
import img5 from "../../../public/assets/images/img5.png";
import { useTranslation } from "react-i18next";
const Second = () => {
  const { t } = useTranslation();
  return (
    <div className={css.second}>
      <div className={css.div}>
        <div className={css.imgg}>
          <Image src={img3} alt="img3" />
        </div>
        <div className={css.tt1}>{t("누구나 사용 가능한 의료 솔루현 메딕으로 새로운 일상을 경험해 보세요.")}</div>
        <div className={css.tt2}>
          <span>{t("더 알아보기")}</span>
          <div className={css.arrow}>
            <Image src={img5} alt="img5" />
          </div>
        </div>
      </div>
      <div className={css.left}>
        <div className={css.img1}>
          <Image src={img1} alt="img1" />
        </div>
        <div className={css.img2}>
          <Image src={img2} alt="img2" />
        </div>
      </div>
      <div className={css.right}>
        <div className={css.img4}>
          <Image src={img4} alt="img4" />
        </div>
        <div className={css.img41}>
          <Image src={img1} alt="img1" />
        </div>
      </div>
    </div>
  );
};

export default Second;
