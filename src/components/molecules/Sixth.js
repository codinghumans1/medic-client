import React from "react";
import Image from "next/image";
import css from "../../../styles/Sixth.module.css";
import img from "../../../public/assets/images/sixth1.png";
import icon from "../../../public/assets/images/img5.png";

const Sixth = () => {
  return (
    <div className={css.sixth}>
      <div className="container">
        <div className={css.sixth11}>
          <div className={css.text1}>신고 서비스</div>
          <div className={css.text2}>응급 원터치 신고로</div>
          <div className={css.text3}>
            위기 상황에 빠르게 대응해요<span>.</span>
          </div>
        </div>
      </div>
      <div className="container2">
        <div className={css.sixth12}>
          <div style={{ paddingInline: "4em" }} className="container">
            <div className={css.sixth121}>
              <div className={css.img}>
                <Image src={img} alt="img" />
              </div>
              <div className={css.sixth122}>
                <div className={css.text121}>응급 신고 간단화</div>
                <div className={css.text122}>
                  주치의, 119 응급센터의 버튼 클릭으로 사전 동의 정보 (혈액형,
                  위치, 국가, 복용약품 등) 응급 기관 문자전달 시스템으로 기존의
                  국적 확인부터 언어 지원까지 과정을 단축 시킵니다.
                </div>
                <div className={css.text123}>
                  <span>더 알아보기</span>
                  <div className={css.icon}>
                    <Image src={icon} alt="img" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Mobile */}
        <div className={css.mobile}>
          <div className={css.mobile1}>
            <span>신고 서비스</span>
            <div className={css.mobile11}>응급 원터치 신고로</div>
            <div className={css.mobile12}>
              위기 상황에 빠르게 대응해요<span>.</span>
            </div>
          </div>
          <div className={css.mobile2}>
            <div className={css.mob_img}>
              <Image src={img} alt="img" />
            </div>
          </div>
          <div className={css.mobile3}>
            <div className={css.text121}>응급 신고 간단화</div>
            <div className={css.text122}>
              주치의, 119 응급센터의 버튼 클릭으로
              <br /> 사전 동의 정보 (혈액형, 위치, 국가, 복용약품 등)
              <br /> 응급 기관 문자전달 시스템으로 기존의
              <br /> 국적 확인부터 언어 지원까지 <br />
              과정을 단축 시킵니다.
            </div>
            <div className={css.text123}>
              <span>더 알아보기</span>
              <div className={css.icon}>
                <Image src={icon} alt="img" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sixth;
