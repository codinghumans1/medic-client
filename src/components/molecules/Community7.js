import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/community71.png";
import img2 from "../../../public/assets/images/community72.png";
import img3 from "../../../public/assets/images/community73.png";
import img4 from "../../../public/assets/images/community74.png";
import img5 from "../../../public/assets/images/community75.png";
import img6 from "../../../public/assets/images/community76.png";
import css from "../../../styles/Community7.module.css";
const Community7 = () => {
  return (
    <div className={css.community7}>
      <div className="container">
        <div className={css.community71}>
          <p>국제화연구소와</p>
          <p>함께하는 회사</p>
        </div>
      </div>
      <div className="container">
        <div className={css.community72}>
          <div className={css.img71}>
            <Image src={img1} alt="img1" />
          </div>
          <div className={css.img72}>
            <Image src={img2} alt="img2" />
          </div>
          <div className={css.img73}>
            <Image src={img3} alt="img3" />
          </div>
          <div className={css.img74}>
            <Image src={img4} alt="img4" />
          </div>
          <div className={css.img75}>
            <Image src={img5} alt="img5" />
          </div>
          <div className={css.img76}>
            <Image src={img6} alt="img6" />
          </div>
        </div>
      </div>
      <div className={css.com_mob71}>
        <div className={css.img71}>
          <Image src={img1} alt="img1" />
        </div>
        <div className={css.img72}>
          <Image src={img2} alt="img2" />
        </div>
        <div className={css.img73}>
          <Image src={img3} alt="img3" />
        </div>
      </div>
      <div className={css.com_mob72}>
        <div className={css.img74}>
          <Image src={img4} alt="img4" />
        </div>
        <div className={css.img75}>
          <Image src={img5} alt="img5" />
        </div>
        <div className={css.img76}>
          <Image src={img6} alt="img6" />
        </div>
      </div>
    </div>
  );
};

export default Community7;
