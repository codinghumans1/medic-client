import React from "react";
import css from "../../../styles/Community3.module.css";
const Community3 = () => {
  return (
    <div className={css.community3}>
      <div
        style={{ display: "flex", justifyContent: "center" }}
        className="container"
      >
        <div className={css.community31}>국제화연구소가 시작하는 사회변화</div>
      </div>
      <div className="container">
        <div className={css.community_mob31}>
          국제화연구소가
          <br /> 시작하는
          <br /> 사회작변화
        </div>
      </div>
    </div>
  );
};

export default Community3;
