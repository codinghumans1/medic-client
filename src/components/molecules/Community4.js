import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/community41.png";
import img2 from "../../../public/assets/images/community42.png";
import css from "../../../styles/Community4.module.css";
const Community4 = () => {
  return (
    <div className={css.community4}>
      <div style={{ display: "flex" }} className="container">
        <div className={css.community41}>
          <Image src={img1} alt="img" />
          <div className={css.community412}>
            <p>메딕프리터</p>
            <p>이주경험 있는 선배 이주민</p>
            <p>(결혼 이주여성, 외국인 근로자 등)으로</p>
            <p>초기정착 과정 이주민의 의료생활을 돕습니다.</p>
          </div>
        </div>
        <div className={css.community42}>
          <div className={css.community421}>
            이주민의 일자리를
            <br /> 창출합니다
          </div>
          <div className={css.community422}>
            <p>다문화센터, 지역사회기관과 연계해 사회 이주 경험있는</p>
            <p>누구나 영상통화대리보호자 ‘메딕프리터’ 로</p>
            <p>활동하며 복지 대상자가 아닌 사회경제</p>
            <p>구성원으로 성장할 수 있습니다.</p>
          </div>
          <div className={css.community423}>
            <p>국제화연구소는</p>
            <p>약 66만 명의 이주목적 외국인들이 대한민국 주권자로</p>
            <p>인정받는 사회를 위해 노력합니다.</p>
          </div>
          <div className={css.community424}>
            <Image src={img2} alt="img" />
          </div>
        </div>
      </div>
      {/* Mobile */}
      <div className={css.com_mob41}>
        <Image src={img1} alt="img" />
      </div>
      <div className={css.com_mob42}>
        <div className={css.com_mob421}>
          이주민
          <br />
          일자리창출
        </div>
        <div className={css.com_mob422}>
          다문화센터, 지역사회기관과 연계해
          <br />
          사회이주 경험이 있는 누구나 ‘메딕프리터’로
          <br />
          활동하며 복지 대상자가 아닌
          <br />
          사회경제 구성원으로
          <br />
          성장할 수 있습니다.
        </div>
        <div className={css.com_mob423}>
          국제화연구소는
          <br />약 66만 명의 이주목적 외국인들이
          <br />
          대한민국 주권자로 인정받는
          <br />
          사회를 위해 노력합니다.
        </div>
      </div>
      <div className={css.com_mob43}>
        <Image src={img2} alt="img" />
        <div className={css.com_mob431}>메딕프리터</div>
        <div className={css.com_mob432}>
          이주경험 있는 선배 이주민
          <br />
          (결혼 이주여성
          <br />
          외국인 근로자 등)으로
          <br />
          초기정착 과정 이주민의
          <br />
          의료생활을 돕습니다.
        </div>
      </div>
    </div>
  );
};

export default Community4;
