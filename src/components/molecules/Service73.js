import React from "react";
import Image from "next/image";
import img11 from "../../../public/assets/images/service711.png";
import img12 from "../../../public/assets/images/service712.png";
import css from "../../../styles/Service73.module.css";

const Service73 = () => {
  return (
    <div className={css.service73}>
      <div className={css.service731}>
        <Image src={img11} alt="img1" />
      </div>
      <div className={css.service732}>
        <span className={css.span1}>
          시기와 통증 강도를
          <br />
          선택해 픽토그램 진찰서를 완성하세요
        </span>
        <span className={css.span2}>
          시기와 통증 강도를 선택해
          <br /> 픽토그램 진찰서를 완성하세요
        </span>
      </div>
    </div>
  );
};

export default Service73;
