import React from "react";
import Image from "next/image";
import css from "./Community8.module.css";
import img516 from "../../../public/img/메딕 아이콘_아이콘@3x.png";

const Community8 = ({ p1, p2, p3, btn }) => {
  return (
    <div className={css.community8}>
      <div
        style={{
          height: "100%",
          display: "flex",
          alignItems: "center",
        }}
        className="container"
      >
        <div className={css.div_parent}>
          <div className={css.community81}>KOREAN LIFE MEDIC</div>
          <div className={css.community82}>
            <div className={css.community821}>
              <p>{p1}</p>
              <p>{p2}</p>
              <p>{p3}</p>
            </div>
            <div className={css.community822}>
              <button>문의하기</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Community8;
