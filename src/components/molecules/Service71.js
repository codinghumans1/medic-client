import React from "react";
import Image from "next/image";
import css from "../../../styles/Service71.module.css";

const Service71 = ({ img, text }) => {
  return (
    <div className={css.service71}>
      <div className={css.imgg}>
        <Image src={img} alt="img4" />
      </div>
      <p className={css.p1}>{text}</p>
    </div>
  );
};

export default Service71;
