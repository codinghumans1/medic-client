import React from "react";
import Image from "next/image";
import css from "../../../styles/Service9.module.css";
import img1 from "../../../public/assets/images/service91.png";
import img2 from "../../../public/assets/images/service92.png";
import img_mob2 from "../../../public/assets/images/service_mob93.png";
const Service9 = () => {
  return (
    <div className={css.service9}>
      <div className="container2">
        <div className={css.parent_div}>
          <div className={css.service91}>
            통역도
            <br /> 진료도
            <br /> 모두 메딕으로
          </div>
          <div className={css.service92}>
            <Image src={img2} alt="img2" />
          </div>
        </div>
      </div>
      <div className="container">
        <div className={css.service_mob91}>
          <p>통역도</p>
          <p>진료도</p>
          <p className={css.p3}>메딕으로</p>
        </div>
      </div>
      <div className={css.service_mob92}>
        <Image src={img_mob2} alt="img_mob2" />
      </div>
    </div>
  );
};

export default Service9;
