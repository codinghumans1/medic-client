import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/community51.png";
import css from "../../../styles/Community5.module.css";
const Community5 = () => {
  return (
    <div className={css.community5}>
      <div className="container">
        <div className={css.community51}>
          <div className={css.community511}>
            <p>이주민이</p>
            <p>행복한 환경을 만듭니다</p>
          </div>
          <div className={css.community512}>
            <Image src={img1} alt="img1" />
            <div className={css.community5121}>
              <div className={css.community51211}>이주민 건강권 보장</div>
              <div className={css.community51212}>
                <p>
                  국내 거주 외국인 의료생활 안전화를 시작으로 베트남, 미국
                  등으로 서비스를 확장해
                </p>
                <p>
                  세계적으로 초기정착 과정에 있는 모든 이주민의 건강권을
                  보장하고자 합니다.
                </p>
                <p>이주민이 행복한 환경을 위한 필수 지표가 되겠습니다.</p>
              </div>
              <div className={css.community_mob51212}>
                국내 거주 외국인 의료생활 안전화를 시작으로 베트남, 미국
                등으로 서비스를 확장해 세계적으로 초기정착 과정에 있는 모든
                이주민의 건강권을 보장하고자 합니다.이주민이 행복한 환경을 위한
                필수 지표가 되겠습니다.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Community5;
