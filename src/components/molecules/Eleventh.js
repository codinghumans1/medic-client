import Image from "next/image";
import React from "react";
import css from "../../../styles/Eleventh.module.css";
import insta from "../../../public/assets/images/insta.png";

const Eleventh = () => {
  return (
    <div className={css.eleventh}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: "100%",
        }}
        className="container"
      >
        <h4>MEDIC</h4>
        <div className={css.parent11}>
          <div className={css.child11}>
            <strong>이용문의</strong>
            <p>internationthink@naver.com</p>
          </div>
          <div className={css.child11}>
            <strong>대표</strong>
            <p>오준재</p>
            <p>Email: happyojj99@naver.com</p>
            <p>Tel : 010-5462-4763 </p>
            <p>사업자 등록번호 : 828-61-00490</p>
          </div>
          <div className={css.child11}>
            <strong>주소</strong>
            <p>경희대 지점</p>
            <p>서울시 동대문구 경희대로 26,</p>
            <p>삼의원창업센터 308호</p>
            <div className={css.thirdDiv4p}>
              <p>삼육대지점</p>
              <p>서울시 노원구 화랑대로 815,</p>
              <p>삼육대학교 학생창업보육센터 B3(백주년기념관 지하)</p>
            </div>
          </div>
          <div className={css.child11}>
            <strong>약관</strong>
            <p>사회성과측정</p>
            <p>개인정보 처리방침</p>
            <p>위치정보동의</p>
          </div>
          <div className={css.insta}>
            <strong>Instagram</strong>
            <div className={css.insta_img}>
              <Image src={insta} alt="insta" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Eleventh;
