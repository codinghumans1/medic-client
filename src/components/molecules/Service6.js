import React from "react";
import Image from "next/image";
import img from "../../../public/assets/images/sixth1.png";
import img2 from "../../../public/assets/images/service6.png";
// import img3 from "../../../public/assets/images/service_mob62.png";
import css from "../../../styles/Service6.module.css";

const Service6 = () => {
  return (
    <div className={css.service6}>
      {/* 1st part*/}
      <div className="container">
        <div className={css.service61}>
          <p className={css.p1}>신고 서비스</p>
          <p className={css.p2}>응급 원터치 신고로</p>
          <p className={css.p3}>위기 상황에 빠르게 대응해요</p>
        </div>
      </div>
      <div className="container2">
        {/* 2nd part */}
        <div className={css.service62}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              paddingInline: "4em",
            }}
            className="container"
          >
            <div className={css.service621}>
              <Image src={img} alt="img" />
            </div>
            <div className={css.service622}>
              <div className={css.service6221}>응급 신고 간단화</div>
              <div className={css.service6222}>
                주치의, 119 응급센터의 버튼 클릭으로 사전 동의 정보 를 응급 기관
                문자전달 시스템으로 기존의 국적 확인부터 언어 지원까지 과정을
                단축 시킵니다.
              </div>
            </div>
          </div>
        </div>
        {/* Mobile view of 2nd part */}
        <div className={css.service_mob62}>
          <div className={css.service_mob621}>
            <Image src={img} alt="img" />
          </div>
        </div>
        {/* 3rd part */}
        <div className={css.service63}>
          <div className={css.service631}>
            <div className={css.service6311}>
              <p>응급신고 10초 후에</p>
              <p>
                <span>119 자동 연결</span> 및 사용자
              </p>
              <p>전담 주치의에게 연결됩니다.</p>
            </div>
            {/* Mobile */}
            <div className={css.service_mob6311}>
              <p>원터치 응급신고 10초 후에</p>
              <p className={css.white_red}>119로 자동 연결 후</p>
              <p>이용자 전담 주치의에게 연결됩니다.</p>
            </div>
          </div>
          <div className={css.service632}>
            <div className={css.service6321}>119 및 주치의 정보알림 내용</div>
            <div className={css.service6322}>
              사용자의 복용약 혈액형, 국가, 위치 등의 사전 동의 정보가 119와
              담당 주치의에게 전달됩니다.
            </div>
          </div>
        </div>
        {/* Mobile */}
        <div className={css.service_mob622}>
          <div className={css.service_mob6221}>응급 신고 간단화</div>
          <div className={css.service_mob6222}>
            주치의, 119 응급센터의 버튼 클릭으로
            <br /> 사전 동의 정보 (혈액형, 위치, 국가, 복용약품 등)
            <br /> 응급 기관 문자전달 시스템으로 기존의
            <br /> 국적 확인부터 언어 지원까지
            <br /> 과정을 단축 시킵니다.
          </div>
        </div>
      </div>
    </div>
  );
};

export default Service6;
