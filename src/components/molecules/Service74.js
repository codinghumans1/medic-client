import React from "react";
import img15 from "../../../public/assets/images/service715.png";
import Image from "next/image";
import css from "../../../styles/Service74.module.css";

const Service74 = () => {
  return (
    <div className={css.service74}>
      <div
        style={{
          paddingInline: "4em",
          display: "flex",
          justifyContent: "space-between",
        }}
        className="container"
      >
        <div className={css.service741}>픽토그램 진찰서 완성 !</div>
        <div className={css.service742}>
          <Image src={img15} alt="img15" />
        </div>
        <div className={css.service743}>
          완성된 픽토그램 진찰서로
          <br /> 원활한 진료가 가능해요
        </div>
      </div>
      <div className={css.service_mob742}>
        <div className={css.text}>픽토그램 진찰서 완성 !</div>
        <Image src={img15} alt="img15" />
      </div>
    </div>
  );
};

export default Service74;
