import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/community61.png";
import css from "../../../styles/Community6.module.css";
const Community6 = () => {
  return (
    <div className={css.community6}>
      <div className={css.community61}>
        <div className="container">
          <div className={css.community611}>
            <p>국제화연구소는</p>
            <p>성장합니다.</p>
          </div>
        </div>
      </div>
      <div className="container">
        <div className={css.community62}>
          <div className={css.community621}>
            <p>2018</p>
            <p>2020</p>
            <div className={css.p2021}>
              2021<div className={css.circle6}></div>
            </div>
          </div>
          <div className={css.community622}>
            <div className={css.community6221}>
              <p style={{ marginLeft: 0 }}>Korean life 탄생</p>
              2020 2nd. SYU Pitching DAY <br />
              삼육대 크리에이터창업경진대회 최우수상
            </div>
            <div className={css.community6222}>
              <p>노원그린캠퍼스 창업콘테스트 KoreanLife Medic 입상</p>
              <p>전국대학생 sw창업 아이디어톤 대회 KoreanLife Medic 최우수상</p>
              <p>예비창업패키지 선정</p>
              <div className={css.lastp}>
                2021 아산나눔재단 아산상회 3기 활동
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Mobile */}
      <div className="container">
        <div className={css.com_mob62}>
          <div className={css.com_mob621}>
            <div className={css.com_mob6211}>
              <span>2018</span> <p>Korean life 탄생</p>
            </div>
            <div className={css.com_mob6211}>
              <span>2020</span>{" "}
              <p>
                2020 2nd. SYU Pitching DAY <br />
                삼육대 크리에이터창업경진대회 최우수상
              </p>
            </div>
          </div>
          <div className={css.com_mob622}>
            <div className={css.circle_mob6}></div>{" "}
            <span className={css.span}>2021</span>
          </div>
          <div className={css.com_mob623}>
            <div className={css.com_text61}>
              노원그린캠퍼스 창업콘테스트
              <br />
              KoreanLife Medic 입상
            </div>
            <div className={css.com_text62}>
              전국대학생 sw창업 아이디어톤 대회
              <br />
              KoreanLife Medic 최우수상
            </div>
            <div className={css.com_text63}>예비창업패키지 선정</div>
            <div className={css.com_text64}>
              2021 아산나눔재단 아산상회 3기 활동
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Community6;
