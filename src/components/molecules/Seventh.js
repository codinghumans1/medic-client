import React from "react";
import Image from "next/image";
import css from "../../../styles/Seventh.module.css";
import img1 from "../../../public/assets/images/seventh_mob.png";
import img2 from "../../../public/assets/images/seventh2.png";
import img3 from "../../../public/assets/images/seventh3.png";
import icon from "../../../public/assets/images/img5.png";
import mobile from "../../../public/assets/images/mobile7th.png";
import seventh3formobile from "../../../public/assets/images/seventh3for_mobile.jpg";

const Seventh = () => {
  return (
    <div className={css.seventh}>
      <div className="container">
        <div className={css.seventh1}>
          <span className={css.p1}>픽토그램 서비스</span>
          <span className={css.p2}>의료 픽토그램으로</span>
          <span className={css.p3}>직관적으로</span>
          <span className={css.p4}>
            나의 증상을 표현하세요<span>.</span>
          </span>
        </div>
      </div>
      <div className="container2">
        <div className={css.seventh12}>
          <div className={css.seventh121}>
            <div
              style={{
                paddingInline: "4em",
                display: "flex",
                paddingLeft: "4em",
              }}
              className="container"
            >
              <div className={css.mob_img}>
                <Image src={img1} alt="mobile" />
              </div>
              <div className={css.seventh1212}>
                <div className={css.text1}>
                  <p style={{ marginBottom: "-20px" }}>픽토그램으로</p>
                  <p>의료정 상담까지</p>
                </div>
                <div className={css.text2}>
                  의료픽토그램으로 나의 증상을 직관적으로 쉽게 전달합니다.
                  진료과정 언어의 장벽을 메딕이 해결합니다.
                </div>
                <div className={css.text3}>
                  <span>더 알아보기</span>
                  <div className={css.icon}>
                    <Image src={icon} alt="img" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={css.seventh122}>
            <div className={css.seventh1221}>
              <Image src={img2} alt="img2" />
              <div className={css.img2_text}>
                맞춤형 진단서 작성까지 간편하게 메딕으로
              </div>
            </div>
            <div className={css.seventh1222}>
              <div className={css.clock}>
                <Image src={img3} alt="img" />
              </div>
              <div className={css.clock_text}>
                상황에 맞는 의원 확인부터 증상과 발현시기까지 다양한 표현이
                가능해요.
              </div>
            </div>
          </div>
        </div>
        {/* Mobile */}
        <div className={css.mobile7}>
          <div className={css.mobile71}>
            <span>간편한 상담</span>
            <div className={css.mobile711}>의료 픽토그램으로</div>
            <div className={css.mobile712}>
              간편한 의료진 상담 받아보세요<span>.</span>
            </div>
          </div>
          <div className={css.mobile_im}>
            <Image src={mobile} alt="img" />
          </div>
          <div className={css.third}>
            <div className={css.mob_img2}>
              <Image src={seventh3formobile} alt="img2" />
            </div>
            <div className={css.img2_text}>
              맞춤형
              <br /> 진단서 작성까지 간편하게
              <br /> 메딕으로
            </div>
          </div>
          <div className={css.forth}>
            <div className={css.clock4}>
              <Image src={img3} alt="img" />
            </div>
            <div className={css.clock_text4}>
              상황에 맞는 의원 확인부터 증상과
              <br /> 발현시기까지
              <br /> 다양한 표현이 가능해요.
            </div>
          </div>
          <div className={css.mobile3}>
            <div className={css.text1}>픽토그램으로</div>
            <div className={css.text121}>의료진 상담까지</div>
            <div className={css.text122}>
              의료픽토그램으로 나의 증상을 직관적으로 쉽게 전달합니다.
              진료과정의 언어장벽을 메딕이 해결합니다.
            </div>
            <div className={css.text123}>
              <span>더 알아보기</span>
              <div className={css.icon}>
                <Image src={icon} alt="img" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Seventh;
