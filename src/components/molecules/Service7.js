import React from "react";
import Image from "next/image";
import img1 from "../../../public/assets/images/service71.png";
import img5 from "../../../public/assets/images/service75.png";
import img6 from "../../../public/assets/images/service76.png";
import img7 from "../../../public/assets/images/service77.png";
import img8 from "../../../public/assets/images/service78.png";
import img9 from "../../../public/assets/images/service79.png";
import img10 from "../../../public/assets/images/service710.png";
import img13 from "../../../public/assets/images/service713.png";
import img14 from "../../../public/assets/images/service714.png";
import img15 from "../../../public/assets/images/service715.png";
import img16 from "../../../public/assets/images/service716.png";
import css from "../../../styles/Service7.module.css";
import Service71 from "./Service71";
import Service73 from "./Service73";
import Service74 from "./Service74";

const Service7 = () => {
  return (
    <div className={css.service7}>
      {/* First part */}
      <div className="container">
        <div className={css.service71}>
          <p className={css.mob_p1}>간편한 상담</p>
          <p className={css.p1}>픽토그램 서비스</p>
          <p className={css.p2}>의료 픽토그램으로</p>
          <p className={css.p3}>직관적으로</p>
          <p className={css.p4}>나의 증상을 표현하세요</p>
          <p className={css.mob_p3}>간편한 의료진 상담 받아보세요</p>
        </div>
      </div>
      <div className="container2">
        {/* 2nd part */}
        <div className={css.service72}>
          <div
            style={{ paddingInline: "4em", display: "flex" }}
            className="container"
          >
            <div className={css.service721}>
              <div className={css.mob_721}>해당하는 증상을 선택하세요</div>
              <Image src={img1} alt="img1" />
            </div>
            <div className={css.service722}>
              해당하는
              <br /> 증상을 선택하세요
            </div>
            <div className={css.service723}>
              <div className={css.service7231}>
                <Service71 img={img5} text="치과" />
                <Service71 img={img6} text="내과" />
                <Service71 img={img7} text="안과" />
              </div>
              <div className={css.service7231}>
                <Service71 img={img8} text="정형외과" />
                <Service71 img={img9} text="산부인과" />
                <Service71 img={img10} text="피부과" />
              </div>
            </div>
          </div>
        </div>
        <div className={css.service_mob7}>
          <div className={css.service_mob721}>
            <div className={css.mob_721}>해당하는 증상을 선택하세요</div>
            <Image src={img1} alt="img1" />
          </div>
        </div>
        {/* 3rd part */}
        <Service73 />
        {/* 4th part */}
        <Service74 />
        {/* 5th part of mobile view */}
        <div className={css.service75}>
          <div className={css.service751}>
            픽토그램으로
            <br /> 의료진 상담까지
          </div>
          <div className={css.service752}>
            의료픽토그램으로 나의 증상을 직관적으로
            <br /> 쉽게 전달합니다. 진료과정의 언어장벽을
            <br /> 메딕이 해결합니다.
          </div>
        </div>
      </div>
    </div>
  );
};

export default Service7;
