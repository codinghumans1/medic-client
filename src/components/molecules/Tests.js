import React from 'react';
import Test from '../atoms/Test';

/**
 * @author
 * @function Tests
 **/

const data = [{ title: 'Helo' }, { title: 'Helo1' }, { title: 'Helo2' }];
const Tests = (props) => {
  return (
    <div>
      {data.map((item, index) => {
        return <Test key={index} title={item.title} />;
      })}
    </div>
  );
};

export default Tests;
