import "../styles/globals.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../src/language/i18n";
import { LayoutWrapper } from "../src/layouts/LayoutWrapper";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  return (
    <LayoutWrapper>
      <Head>
        <link rel="preload" href="/fonts/notosanscjkr/NotoSansCJKkr-Black (TTF).ttf" as="font" crossOrigin="" />
        <link rel="preload" href="/fonts/pretendard/Pretendard-Bold.ttf" as="font" crossOrigin="" />
      </Head>
      <Component {...pageProps} />
    </LayoutWrapper>
  );
}

export default MyApp;
